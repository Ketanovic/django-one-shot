from django.shortcuts import render
from todos.models import TodoList


# Create your views here.
def todo_list_list(request):
    todo_list_lists = TodoList.objects.all()
    context = {"todo_list_list": todo_list_lists}
    return render(request, "todos/list.html", context)
